FROM registry.fedoraproject.org/fedora:latest as builder

WORKDIR /src/
RUN dnf -y --refresh install emacs-nox yarnpkg nodejs
COPY . /src/
RUN yarn install
RUN set -e; mkdir -p public/fontawesome-free; \
    cp --recursive node_modules/@fortawesome/fontawesome-free/* public/fontawesome-free/
RUN emacs -Q --script build.el

FROM docker.io/library/nginx:latest

COPY nginx.default.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /src/public/ /usr/share/nginx/html/
EXPOSE 5050
